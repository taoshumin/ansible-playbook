### Mac 手册手册

- 安装

````shell
brew search ansible
````

- 查看安装

```shell
╰─± ansible --version
ansible [core 2.13.1]
  config file = None
  configured module search path = ['/Users/taoshumin_vendor/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/homebrew/Cellar/ansible/6.1.0/libexec/lib/python3.10/site-packages/ansible
  ansible collection location = /Users/taoshumin_vendor/.ansible/collections:/usr/share/ansible/collections
  executable location = /opt/homebrew/bin/ansible
  python version = 3.10.6 (main, Aug 30 2022, 04:58:14) [Clang 13.1.6 (clang-1316.0.21.2.5)]
  jinja version = 3.1.2
  libyaml = True
```

- 设置配置文件路径 (`~/.ansible/ansible.cfg`)

```shell
[defaults]
# some basic default values...
 
inventory      = /etc/ansible/hosts
#library        = /usr/share/my_modules
```

- `/etc/ansible/hosts`

```shell
[aliyun]
aliyun-server-1  ansible_user=root
aliyun-server-2  ansible_user=root
aliyun-server-3  ansible_user=root
aliyun-server-4  ansible_user=root
aliyun-server-5  ansible_user=root
aliyun-server-6  ansible_user=root
aliyun-server-7  ansible_user=root
aliyun-server-8  ansible_user=root
aliyun-server-9  ansible_user=root
aliyun-server-10 ansible_user=root
```

- 秘钥登录

```shell
ssh-copy-id -i ~/.ssh/id_rsa.pub taoshumin_vendor@192.168.1.8
ssh -i ~/.ssh/id_rsa.gridsum root@aliyun-server-1
```

- 相关命令操作

```shell
ansible aliyun -m shell -a "sudo apt install psmisc -y"
ansible aliyun -m shell -a "mkdir chegg"
ansible aliyun -m shell -a "chdir=/root/chegg chmod +x chegg"
ansible aliyun --private-key=~/.ssh/id_rsa.gridsum -m shell -a 'chdir=/data/cpsdb rm -rf *'
ansible aliyun -b -m copy -a "src=/Users/taoshumin_vendor/go/src/yho.io/chegg/dist/cheggd_linux_amd64/chegg dest=/root/chegg"
ansible aliyun -b -m copy -a "src=/Users/taoshumin_vendor/go/src/yho.io/chegg/config/flashcard/chegg-vultr-sub-flashcard.yaml dest=/root/chegg"
ansible aliyun -m shell -a "killall chegg"
ansible aliyun -m shell -a "chdir=/root/chegg nohup ./chegg -f chegg-vultr-sub-flashcard.yaml > chegg.log &"
```

### Ansible 一键部署


> **安装 Ansible 和 Git**

```
$ yum install ansible git -y
```

> **修改 hosts**

```shell script
$ cd deployed
$ vi hosts
-----------
# A collection of hosts belonging to the 'all' group
[all]
cpsdb-meta1

[meta]
cpsdb-meta-1
cpsdb-meta-2
cpsdb-meta-3
[data]
cpsdb-data-1
cpsdb-data-2
cpsdb-data-3
cpsdb-data-4
```

> **修改节点服务器/etc/hosts**

```shell script
$ cd /roles/init/templates
$ vi hosts
-----------
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

10.200.60.88    cpsdb-meta1     cpsdb-meta1
10.200.60.73    cpsdb-meta2     cpsdb-meta2
10.200.60.58    cpsdb-meta3     cpsdb-meta3
10.200.60.30    cpsdb-data1     cpsdb-data1
10.200.60.38    cpsdb-data2     cpsdb-data2
10.200.60.71    cpsdb-data3     cpsdb-data3
10.200.60.114   cpsdb-data4     cpsdb-data4
```
>  **修改变量group_vars**

```shell script
$ cd group_vars
---
cpsdb:
  install_package_name: cpsdb-1.0.0-1.el7.centos.x86_64.rpm    #[本次安装包文件名]
  uninstall_package_name: cpsdb-1.0.0-1.el7.centos.x86_64      #[卸载安装包名]
  service_name: cpsdb                                          #[可执行程序名；默认cpsdb]
  group_user: cpsdb                                            #[cpsdb的group名；cat /etc/passd]
  etc_config: /opt/cpsdb/etc/cpsdb.conf                         #[cpsdb配置文件路径]
meta: 
  join: cpsdb-meta1:8091,cpsdb-meta2:8091,cpsdb-meta3:8091     #[meta配置文件join]
```

> ** 安装**

```shell script
$ ansible-playbook -i hosts site.yml
```

> **卸载**

```shell script
$ ansible-playbook -i hosts uninstall.yml
```
